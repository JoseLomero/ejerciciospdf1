// Funciones generales

/**
 * Crea un elemento y lo añade al padre
 * @param {HTMLCollection} parent Padre en el cual se creará el elemento
 * @param {HTMLCollection} child Tipo de elemento que queremos crear
 * @param {Text} text Texto que se le añadirá al elemento creado
 * @param {attributes} attributes Atributos que se añadirán a nuestro elemento
 */
function addElement(parent, child, text, attributes) {
	var childElement = document.createElement(child);
	if (text != undefined) {
		var contenido = document.createTextNode(text);
		childElement.appendChild(contenido);
	}

	if (attributes != undefined && attributes instanceof Array) {
		for (var i = 0; i < attributes.length; i++) {
			var attrName = attributes[i].split("=")[0];
			var attrValue = attributes[i].split("=")[1];
			childElement.setAttribute(attrName, attrValue);
		}
	}
	parent.appendChild(childElement);
	return childElement;
}

/**
 * Nos pasan un numero y decimos si es par o impar
 * @param {number} num Numero que se nos pasa
 */
function par(num) {
    if (num%2 == 0) {
        return num + " es par";
    } else {
        return num + " es impar";
    }
}

/**
 * Calcula el numero primo, nos devuelve true si es primo, false si no lo es
 * @param {number} x numero que calcularemos para ver si es primo
 */
function primo(x) {
    if (x <= 1)
        return false;

    if (x <= 3)
        return true;

    if (x % 2 == 0 || x % 3 == 0)
        return false;

    for (var i = 5; i * i <= x; i = i + 6) {
        if (x % i == 0 || x % (i + 2) == 0)
            return false;
    }

    return true;
}


// Funciones de los botones

function mayor01() {
    var ejercicio = document.getElementById("primero");
    var respuesta1 = document.getElementsByClassName("respuesta")[0].value;
    var respuesta2 = document.getElementsByClassName("respuesta")[1].value;

    var respuestaFinal = mayor(respuesta1, respuesta2);
    addElement(ejercicio, "p", respuestaFinal);
}

function palabraDetalles2() {
    var ejercicio = document.getElementById("segundo");
    var pregunta = document.getElementsByClassName("respuesta")[2].value;
    var respuesta = palabraDetalles(pregunta);

    for (let i = 0; i < respuesta.length; i++) {
        addElement(ejercicio, "p", respuesta[i]);
    }
}

function sumar3() {
    var ejercicio = document.getElementById("tercero");
    
    var respuesta = document.getElementsByClassName("respuesta")[3];
    respuesta = respuesta.value.split(", ");
    var nuevaRespuesta = [];
    for (let i = 0; i < respuesta.length; i++) {
        nuevaRespuesta.push(parseInt(respuesta[i]));
        
    }

    var respuestaFinal = sumaValores(nuevaRespuesta);
    addElement(ejercicio, "p", respuestaFinal);
}

function queDiaEs() {
    var ejercicio = document.getElementById("cuartox")
    var respuesta = hoy();
    addElement(ejercicio, "p", respuesta);
}

function sacarPrimoCifras5() {
    var ejercicio = document.getElementById("quinto");
    var pregunta = document.getElementsByClassName("respuesta")[5].value;

    var respuestaFinal = primoCifras(pregunta);

    addElement(ejercicio, "p", "Este es el resultado " + respuestaFinal);
}