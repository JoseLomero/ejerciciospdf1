// Ejercicios

/**
 * Ejercicio 1
 * Nos dará el numero mayor entre a y b
 * @param {number} a Primer numero que nos dan
 * @param {number} b Segundo numero que nos dan
 */
function mayor(a, b) {
    a = parseInt(a);
    b = parseInt(b);
    if (a == b) {
        return "Son el mismo número, no me engañas"
    } else if (a > b) {
        return total = a + " es mayor a " + b;
    } else {
        return total = b + " es mayor a " + a;
    }
}

/**
 * Ejercicio 3
 * Sumaremos los valores de todos los numeros de la array dada
 * @param {array} arr Array que contiene los numeros a sumar
 */
function sumaValores(arr) {
    var total = 0;
    for (let i = 0; i < arr.length; i++) {
        total = total + arr[i];
    }
    return total;
}

/**
 * Ejercicio 2
 * Nos indicará cuantas letras tiene la palabra, vocales, consonantes...
 * @param {string} string Palabra de la cual queremos detalles
 */
function palabraDetalles(string) {
    var arr = [];
    var primerTexto = string + " tiene " + string.length + " letras";
    arr.push(primerTexto);
    var segundoTexto = par(string.length);
    arr.push(segundoTexto);
    var vocales = 0, consonantes = 0;
    for (let i = 0; i < string.length; i++) {
        if (string.charAt(i) == "a" || string.charAt(i) == "e" || string.charAt(i) == "i" || string.charAt(i) == "o" || string.charAt(i) == "u") {
            vocales++;
        } else {
            consonantes++;
        }
    }
    var tercerTexto = string + " tiene " + vocales + " vocales";
    var cuartoTexto = string + " tiene " + consonantes + " consonantes";
    arr.push(tercerTexto);
    arr.push(cuartoTexto);
    
    return arr;
}

/**
 * Ejercicio 4
 * Al ejecutar la funcion nos dira el dia actual
 */
function hoy() {
    var day = new Date();
    var dias = ["domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"]
    var respuesta = "Hoy es " + dias[day.getDay()]
    return respuesta;
}

/**
 * Ejercicio 5
 * Le pasamos un numero y nos devolverá el primer numero con las cifras
 * que le hemos pasado que sea primo. Utiliza la funcion primo()
 * @param {number} x Numero de cifras que debe tener el numero
 */
function primoCifras(x) {
    if (x != 1) {
        x--;
        if (x != 1) {
            var num = 1;
            for (let i = 0; i < x; i++) {
                num *= 10;
            }
        } else {
            var num = 10;
        }
    } else {
        var num = 1;
    }

    while (true) {
        var newPrime = primo(num);
        if (newPrime == true) {
            return num;
        }
        num++;
    }
}