function calcula() {
    var num1 = Math.floor(Math.random()*50);
    var num2 = Math.floor(Math.random()*50);
	var num3 = Math.floor(Math.random()*50);
	
	var span1 = document.getElementById("num1");
	var span2 = document.getElementById("num2");
	var span3 = document.getElementById("num3");
	
	addElement(span1, "p", num1, ["class=numeros"]);
	addElement(span2, "p", num2, ["class=numeros"]);
	addElement(span3, "p", num3, ["class=numeros"]);
}




/**
 * Crea un elemento y lo añade al padre
 * @param {HTMLCollection} parent Padre en el cual se creará el elemento
 * @param {HTMLCollection} child Tipo de elemento que queremos crear
 * @param {Text} text Texto que se le añadirá al elemento creado
 * @param {attributes} attributes Atributos que se añadirán a nuestro elemento
 */
function addElement(parent, child, text, attributes) {
	var childElement = document.createElement(child);
	if (text != undefined) {
		var contenido = document.createTextNode(text);
		childElement.appendChild(contenido);
	}

	if (attributes != undefined && attributes instanceof Array) {
		for (var i = 0; i < attributes.length; i++) {
			var attrName = attributes[i].split("=")[0];
			var attrValue = attributes[i].split("=")[1];
			childElement.setAttribute(attrName, attrValue);
		}
	}
	parent.appendChild(childElement);
	return childElement;
}