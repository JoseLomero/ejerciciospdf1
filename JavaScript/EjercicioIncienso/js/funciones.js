function filtraCorreo(correo) {
    console.log(correo.value);

    if (correo.value.length < 6) {
        createError("Email demasiado corto");
        return false;
    } else if (correo.value.length > 50) {
        createError("Email demasiado largo");
        return false;
    }

    correo.setAttribute("style", "");
    return true;
}