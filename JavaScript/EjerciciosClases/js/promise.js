
let hay_conexion = true;
let hay_datos = false;

// Promise
const conecta = new Promise(
    function (resolve, reject) {
        if (hay_conexion) {
            const conn = {
                server: 'url_servidor',
                error: 'false'
            };
            resolve(conn); // conseguido!
        } else {
            const motivo = new Error('no hay conexión');
            reject(motivo); // reject
        }

    }
);



const obten_datos = function (conn) {
        return new Promise(
            function (resolve, reject) {
                if (hay_datos && conn) {
                    const data = {
                        datos: [1,2,3],
                        error: 'false'
                    };
                    resolve(data); // conseguido!
                } else {
                    var motivo = new Error(`en el servidor ${conn.server} no hay datos`);
                    reject(motivo); // reject
                }
            }
        ); 
        }

const proceso = function () {
    conecta
        .then( (conn) => {
            console.log("conexion obtenida", conn);
            return obten_datos(conn);
        })
        .then( (datos) => {
            console.log("datos obtenidos", datos);
        })
        .catch( (error) => {
            console.log(error.message);
        });
};

proceso();