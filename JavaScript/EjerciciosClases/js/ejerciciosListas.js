// lista de marcas distintas y cuantas motos tiene cada una

// Primer ejercicio
function getPrecio(moto) {
    var precioPartes = moto.preu;
    return precioPartes;
}

function motosPrecios() {
    var maxPrecio = MOTOS[0].preu;
    var minPrecio = MOTOS[0].preu;
    var motoCara, motoBarata;
    var precios = [];

    for (let i = 0; i < MOTOS.length; i++) {

        var precio = getPrecio(MOTOS[i]);
        if (precio > maxPrecio) {
            maxPrecio = precio;
            motoCara = MOTOS[i].model;
        }

        if (precio < minPrecio) {
            minPrecio = precio;
            motoBarata = MOTOS[i].model;
        }
    }

    precios.push("La moto más cara es: " + motoCara);
    precios.push("La moto más barata es: " + motoBarata);

    return precios;
}


// Segundo ejercicio
function motosMarca(marca) {
    var motosMarca = [];
    for (let i = 0; i < MOTOS.length; i++) {
        let marcaMoto = getMarca(MOTOS[i]);
        if (marcaMoto.toUpperCase() == marca.toUpperCase()) {
            motosMarca.push(MOTOS[i]);
        }
    }
    return motosMarca;
}

function motosSemiUsadas() {
    var motosSemi = motosMarca("honda").filter(moto => moto.kilometres < 30000);
    return motosSemi;
}

// Tercer Ejercicio
function motosMenosKM(array, km) {
    var motosMenosKM = array.filter(moto => moto.kilometres < km);
    return motosMenosKM;
}

function masCilindradas(array, cc) {
    var motosMasCC = array.filter(moto => moto.cilindrada > cc);
    return motosMasCC;
}

function motosMenosKMyCC(array, km, cc) {
    var menosKM = motosMenosKM(array, km);
    var total = masCilindradas(menosKM, cc);
    return total;
}

// Cuarto Ejercicio
function entrePrecios(array, menor, mayor) {
    var precios = [];
    for (let i = 0; i < array.length; i++) {
        let moto = array[i];
        if (moto.preu > menor && moto.preu < mayor) {
            precios.push(moto);
        }
    }
    return precios;
}

function ejercicioCuatro(array, km, cc, menor, mayor) {
    var losPrecios = entrePrecios(array, menor, mayor);
    return motosMenosKMyCC(losPrecios, km, cc);
}