class Figura {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class Rectangulo extends Figura {
    constructor(x, y, lado1, lado2) {
        super(x, y);
        this.lado1 = lado1;
        this.lado2 = lado2;
    }

    calculaArea() {
        return this.lado1*this.lado2;
    }
}

class Triangulo extends Figura {
    constructor(x, y, base, altura) {
        super(x, y);
        this.base = base;
        this.altura = altura;
    }

    calcularArea() {
        return (this.base*this.altura)/2;
    }
}

class Cuadrado extends Rectangulo {
    constructor(x, y, lado) {
        super(x, y, lado, lado);
    }
}

// var figuras = new Figura(2, 3)
// var rectangulos = new Rectangulo(10, 20, 30, 40)
// var triangulos = new Triangulo(10, 20, 30, 40)
// var cuadrados = new Cuadrado(1, 2, 30)

// rectangulos.calculaArea()
// triangulos.calcularArea()