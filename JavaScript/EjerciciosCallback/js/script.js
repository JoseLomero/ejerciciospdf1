function cargar() {
    var minimo = $("#minimo").val();
    var posiciones = [];
    minimo = parseInt(minimo);
    $("#marca").empty();

    $.getJSON(
        "https://api.citybik.es/v2/networks/bicing",
        function (data) {
            var estaciones = data.network.stations;

            for (let i = 0; i < estaciones.length; i++) {
                var estacionActual = estaciones[i];
                    if (estacionActual.free_bikes >= minimo) {
                        var fila = $("<tr>");
                        var celdaNombre = $("<td>").text(estacionActual.name);
                        var celdaDisponibles = $("<td>").text(estacionActual.free_bikes);
                        var celdaSlots = $("<td>").text(estacionActual.empty_slots);
                        var celdaLat = $("<td>").text(estacionActual.latitude);
                        var celdaLong = $("<td>").text(estacionActual.longitude);
                        fila.append(celdaNombre, celdaDisponibles, celdaSlots, celdaLat, celdaLong);
                        $("#marca").append(fila);
                        posiciones.push({
                            latitude: estacionActual.latitude,
                            longitude: estacionActual.longitude
                        });
                    }
            }
        });
}