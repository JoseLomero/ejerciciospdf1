/**
 * Nos pasan un numero y decimos si es par o impar
 * @param {number} num Numero que se nos pasa
 */
function par(num) {
    if (num%2 == 0) {
        console.log(num + " es par");
    } else {
        console.log(num + " es impar");
    }
}

/**
 * Nos dará el numero mayor entre a y b
 * @param {number} a Primer numero que nos dan
 * @param {number} b Segundo numero que nos dan
 */
function mayor(a, b) {
    if (a > b) {
        console.log(a + " es mayor a " + b)
    } else {
        console.log(b + " es mayor a " + a)
    }
}

/**
 * Funcion que devuelve datos básicos del numero dado
 * @param {number} x Numero del cual queremos los datos
 */
function datos(x) {
    par(x);

    if (x % 3 == 0) {
        console.log(x + " es divisible entre 3")
    } else {
        console.log(x + " no es divisible entre 3")
    }

    if (x % 5 == 0) {
        console.log(x + " es divisible entre 5")
    } else {
        console.log(x + " no es divisible entre 5")
    }

    if (x % 7 == 0) {
        console.log(x + " es divisible entre 7")
    } else {
        console.log(x + " no es divisible entre 7")
    }
}


/**
 * Sumaremos los valores de todos los numeros de la array dada
 * @param {array} arr Array que contiene los numeros a sumar
 */
function sumaValores(arr) {
    var total = 0;
    for (let i = 0; i < arr.length; i++) {
        total = total + arr[i];
    }
    return console.log(total);
}


/**
 * Calcula el factorial del numero dado
 * @param {number} x Numero del cual calcularemos el factorial
 */
function factorial(x) {
    var total = 1;
    for (i = 1; i <= x; i++) {
        total = total * i;
    }
    return total;
}


/**
 * Calcula el numero primo, nos devuelve true si es primo, false si no lo es
 * @param {number} x numero que calcularemos para ver si es primo
 */
function primo(x) {
    if (x <= 1)
        return false;

    if (x <= 3)
        return true;

    if (x % 2 == 0 || x % 3 == 0)
        return false;

    for (var i = 5; i * i <= x; i = i + 6) {
        if (x % i == 0 || x % (i + 2) == 0)
            return false;
    }

    return true;
}


/**
 * Se le da un numero y te muestra el factorial suyo
 * @param {number} num Numero que calcularemos
 */
function fibonacci(num) {
    var var1 = 0;
    var var2 = 1;
    var var3;
    console.log(var1);
    console.log(var2);

    for (var i = 3; i <= num; i++) {
        var3 = var1 + var2;
        var1 = var2;
        var2 = var3;
        console.log(var3);
    }
}

/**
 * Le pasamos un numero y nos devolverá el primer numero con las cifras
 * que le hemos pasado que sea primo. Utiliza la funcion primo()
 * @param {numer} x Numero de cifras que debe tener el numero
 */
function primoCifras(x) {
    if (x != 1) {
        x--;
        if (x != 1) {
            var num = 1;
            for (let i = 0; i < x; i++) {
                num *= 10;
            }
        } else {
            var num = 10;
        }
    } else {
        var num = 1;
    }

    while (true) {
        var newPrime = primo(num);
        if (newPrime == true) {
            return num;
        }
        num++;
    }
}

/**
 * Devuelve la misma palabra pero con la primera letra en mayuscula
 * @param {string} string Palabra a capitalizar
 */
function capitaliza(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

/**
 * Nos indicará cuantas letras tiene la palabra, vocales, consonantes...
 * @param {string} string Palabra de la cual queremos detalles
 */
function palabraDetalles(string) {
    console.log(string + " tiene " + string.length + " letras");
    par(string.length);
    var vocales = 0, consonantes = 0;
    for (let i = 0; i < string.length; i++) {
        if (string.charAt(i) == "a" || string.charAt(i) == "e" || string.charAt(i) == "i" || string.charAt(i) == "o" || string.charAt(i) == "u") {
            vocales++;
        } else {
            consonantes++;
        }
    }
    console.log(string + " tiene " + vocales + " vocales");
    console.log(string + " tiene " + consonantes + " consonantes");
}

function hoy() {
    var day = new Date();
    var dias = ["domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"]
    console.log("Hoy es " + dias[day.getDay()]);
}


/* Ejercicios hardcodeados */

/*
mayor(3,5);
datos(7);
sumaValores([3, 5, 9]);
console.log(factorial(9));
primo(5);
fibonacci(23);
primoCifras(2);

// Strings
capitaliza("hola");
palabraDetalles("hola");

// Dates
hoy();
*/