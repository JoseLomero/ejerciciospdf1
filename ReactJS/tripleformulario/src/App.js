import React, { Component } from 'react';
import './App.css';

import Botones from './Components/Botones'

class App extends Component {
  render() {
    return(
      <div>
        <Botones />
      </div>
    )
  }
}

export default App;