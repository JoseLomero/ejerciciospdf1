import React, { Component } from 'react';

import UserLayout from '../Layouts/UsuariosLayout'
import ProductLayout from '../Layouts/ProductosLayout'
import CategoryLayout from '../Layouts/CategoriasLayout'

class Botones extends Component {

    state = {
        usuarios: false,
        categorias: false,
        productos: false,
        modificaciones: false
    }

    // activateMods = (newState) => this.setState({modificaciones: estado});

    setOff = () => {
        this.setState({
            usuarios: false,
            categorias: false,
            productos: false
        })
    }

    toggle = (e) => {
        if (!this.state[e.target.name]) {
            this.setOff();
            this.setState({ [e.target.name]: true })
        } else {
            this.setState({ [e.target.name]: false })
        }
    }


    render() {
        return (
            <div className="App m-4">
                <div className="m-2">
                    <button
                        name="usuarios"
                        className="btn btn-primary"
                        onClick={this.toggle}>
                        Usuario</button>
                </div>
                <div className="m-2">
                    <button
                        name="categorias"
                        className="btn btn-warning"
                        onClick={this.toggle}>
                        Categoria</button>
                </div>
                <div className="m-2">
                    <button
                        name="productos"
                        className="btn btn-danger"
                        onClick={this.toggle}>
                        Producto</button>
                </div>

                {this.state.categorias ? <CategoryLayout /> : null}
                {this.state.usuarios ? <UserLayout /> : null}
                {this.state.productos ? <ProductLayout /> : null}
            </div>
        )
    }
}

export default Botones;