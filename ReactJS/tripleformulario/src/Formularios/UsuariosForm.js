import React from 'react';

const UsuarioForm = props => {
    return (
        <div className="container p-4 bg-secondary">
            <form action="" >
                <div className="row justify-content-center">
                
                <h1>Usuario</h1>

                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce el ID</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    autoFocus
                                    type="number"
                                    name="idUsuario"
                                    placeholder="Escribe aqui tu ID"
                                    value={props.usuarioValues.idUsuario}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>


                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce tu Nombre</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    name="nombreUsuario"
                                    placeholder="Escribe aqui tu nombre"
                                    value={props.usuarioValues.nombreUsuario}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce tu email</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    type="email"
                                    name="emailUsuario"
                                    placeholder="Escribe aqui tu email"
                                    value={props.usuarioValues.emailUsuario}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-4">
                    <button
                        type="submit"
                        className="btn btn-primary">
                        Envia</button>
                </div>
            </form>
        </div>
    )
}

export default UsuarioForm;