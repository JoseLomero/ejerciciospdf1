import React from 'react';

const ProductosForm = props => {
    return (
        <div className="container p-4 bg-info">
            <form action="" >
                <div className="row justify-content-center">

                    <h1>Producto</h1>


                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce el ID del Producto</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    autoFocus
                                    type="text"
                                    name="idProducto"
                                    placeholder="Consta de 2 letras y 3 numeros"
                                    value={props.productoValues.idProducto}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>


                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce el precio</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    type="number"
                                    name="precioProducto"
                                    placeholder="Escribe aqui el precio"
                                    value={props.productoValues.precioProducto}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce el ID del País</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    type="number"
                                    name="idPaisProducto"
                                    placeholder="Escribe aqui ID del País"
                                    value={props.productoValues.idPaisProducto}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-4">
                    <button
                        type="submit"
                        className="btn btn-primary">
                        Envia</button>
                </div>
            </form>
        </div>
    )
}

export default ProductosForm;