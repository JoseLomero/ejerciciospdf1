import React from 'react';

const ProductosForm = props => {
    return (
        <div className="container p-4 bg-light">
            <form action="" >
                <div className="row justify-content-center">

                    <h1>Categoria</h1>

                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce el ID</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    autoFocus
                                    type="number"
                                    name="idCategoria"
                                    placeholder="Escribe aqui el ID"
                                    value={props.categoriaValues.idCategoria}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>


                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce el Nombre de la Categoría</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    name="nombreCategoria"
                                    placeholder="Escribe aqui el nombre de Categoria"
                                    value={props.categoriaValues.nombreCategoria}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="row justify-content-center">
                            <div className="col-md-9">
                                <label>Introduce la descripción</label>
                            </div>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    name="descripcionCategoria"
                                    placeholder="Escribe aqui su descripcion"
                                    value={props.categoriaValues.descripcionCategoria}
                                    onChange={props.onChange}></input>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-4">
                    <button
                        type="submit"
                        className="btn btn-primary">
                        Envia</button>
                </div>
            </form>
        </div>
    )
}

export default ProductosForm;