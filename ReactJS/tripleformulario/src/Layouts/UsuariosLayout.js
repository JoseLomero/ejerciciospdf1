import React, { Component } from 'react';

import UsuarioForm from '../Formularios/UsuariosForm'

class UserLayout extends Component {
    state = {
        emailUsuario: '',
        nombreUsuario: '',
        idUsuario: ''
    }

    handleOnChange = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div>
                <UsuarioForm onChange={this.handleOnChange} usuarioValues={this.state} />
            </div>
        )
    }
}

export default UserLayout;