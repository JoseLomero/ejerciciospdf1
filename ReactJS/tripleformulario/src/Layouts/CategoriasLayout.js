import React, { Component } from 'react';

import CategoriasForm from '../Formularios/CategoriasForm'

class CategoryLayout extends Component {
    state = {
        idCategoria: '',
        nombreCategoria: '',
        descripcionCategoria: ''
    }

    handleOnChange = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div>
                <CategoriasForm onChange={this.handleOnChange} categoriaValues={this.state} />
            </div>
        )
    }
}

export default CategoryLayout;