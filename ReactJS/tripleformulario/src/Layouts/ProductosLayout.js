import React, { Component } from 'react';

import ProductosForm from '../Formularios/ProductosForm'

class ProductLayout extends Component {
    state = {
        idProducto: '',
        precioProducto: '',
        idPaisProducto: ''
    }

    handleOnChange = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div>
                <ProductosForm onChange={this.handleOnChange} productoValues={this.state} />
            </div>
        )
    }
}

export default ProductLayout;