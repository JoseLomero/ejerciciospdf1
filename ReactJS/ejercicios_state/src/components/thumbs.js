import React, { Component } from 'react';

import thumbsUP from '../img/thumbs/thumbs-up.png';
import thumbsDOWN from '../img/thumbs/thumbs-down.png';

class Thumbs extends Component {
    state = {
        pulgares: true
    }

    switch = () => {
        this.setState({
            pulgares: !this.state.pulgares
        })
    }

    render() {
        if (this.state.pulgares) {
            return (
                <img src={thumbsUP} onClick={() => this.switch()} />
            )
        } else {
            return (
                <img src={thumbsDOWN} onClick={() => this.switch()} />
            )
        }
    }
}

export default Thumbs;