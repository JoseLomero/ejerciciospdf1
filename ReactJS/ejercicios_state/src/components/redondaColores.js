import React, { Component } from 'react';
import '../styles/redondaColores.css';

class Redonda extends Component {
    constructor() {
        super();
        this.state = {
            color: 0
        }
    }

    procesacolor = e => {
        this.setState({
            color: this.state.color + 1
        })
    }

    render() {
        let colorArray = ['gray', 'red', 'green', 'blue'];
        let numColor = this.state.color % 4;
        return (
            <div
                id="redonda"
                className={colorArray[numColor]}
                onClick={(e) => this.procesacolor()}
            ></div>
        )
    }
}
export default Redonda;