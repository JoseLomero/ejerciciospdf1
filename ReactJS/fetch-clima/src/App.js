import React from 'react';
import './App.css';

import Prevision from './components/prevision';

function App() {
  return (
    <div className="App">
      <Prevision />
    </div>
  );
}

export default App;
