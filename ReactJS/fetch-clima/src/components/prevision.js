import React, { Component } from 'react';

class Prevision extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();
    }

    getPrevisio() {
        const ciutat = "barcelona,es";
        const apiKey = "5e03634420c90cc66e1d131a646e818c";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => {
                this.setState(data)
            })
            .catch(error => console.log(error));


    }

    render() {

        if (!this.state.list.length) {
            return <h1>Cargando datos...</h1>
        }

        var dias = [];
        var newDay = null;
        var dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var minTemp, maxTemp, minCont, maxCont;
        for (let i = 0; i < this.state.list.length; i++) {
            let newFecha = new Date(this.state.list[i].dt * 1000);
            let icono = "http://openweathermap.org/img/w/" + this.state.list[i].weather[0].icon + ".png";

            if (newDay === newFecha.getDay()) {
                // maxTemp += this.state.list[i].main.temp_max;
                // minTemp += this.state.list[i].main.temp_min;
                // minCont++;
                // maxCont++;
            } else {
                newDay = newFecha.getDay();

                dias.push(
                    <div className="col-6 border rounded m-1 bg-light">
                        <p>{dayName[newDay]}</p>
                        <img className="wicon" src={icono} alt="Weather Icon" />
                        <p>Max: {maxTemp/maxCont}º || Min: {minTemp/minCont}º</p>
                    </div>)
            }
        }

        return (
            <div className="container">
                <div className="row justify-content-center">
                    {dias}
                </div>
            </div>
        )
    }
}

export default Prevision;