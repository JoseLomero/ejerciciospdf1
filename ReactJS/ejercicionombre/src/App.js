import React, { Component } from 'react';
import './App.css';
import Form from './components/Form';
import Coordenadas from './components/Coordenadas'

class App extends Component {

  state = {
    formularioActivo: false,
    coordenadasActivas: false
  }

  toggleFormulario = (e) => {
    if (this.state.formularioActivo === true) {
      this.setState({
        formularioActivo: false
      })
    } else {
      this.setState({
        formularioActivo: true
      })
    }
  }

  toggleCoordenadas = (e) => {
    if (this.state.coordenadasActivas === true) {
      this.setState({
        coordenadasActivas: false
      })
    } else {
      this.setState({
        coordenadasActivas: true
      })
    }
  }

  render() {
    return (
      <div>
        <button
          className="btn btn-primary"
          onClick={this.toggleCoordenadas}>
          Coordenadas</button>
        <button
          className="btn btn-warning"
          onClick={this.toggleFormulario}>
          Formulario</button>

        {this.state.formularioActivo ? <Form /> : null}
        {this.state.coordenadasActivas ? <Coordenadas /> : null}
      </div>
    )
  }
}

export default App;