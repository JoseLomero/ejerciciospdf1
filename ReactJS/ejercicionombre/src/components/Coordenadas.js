import React, { Component } from 'react';

class Coordenadas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitud: null,
            longitud: null
        };
    }

    render() {

        return (
            this.state.latitud == null ? (
                <div>CARGANDO</div>
            ) : (
                    <div>
                        <h2>Latitud: {this.state.latitud}</h2>
                        <h2>Longitud: {this.state.longitud}</h2>
                    </div>
                )
        )
    }

    componentDidMount() {
        this.geoId = navigator.geolocation.watchPosition(posicion =>
            this.setState({
                longitud: posicion.coords.longitude,
                latitud: posicion.coords.latitude
            })
        )
    }


    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.geoId);
    }
}

export default Coordenadas;