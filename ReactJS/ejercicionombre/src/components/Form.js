import React, { Component } from 'react';

class Form extends Component {

    state = {
        nombre: '',
        estado: false
    }

    getDatos = (e) => {
        this.setState({
            nombre: e.target.value
        })
    }

    changeDiv = (e) => {
        this.setState({
            estado: true
        })
    }

    render() {
        if (this.state.estado === true) {
            return (
                <div>
                    <p>Bienvenido, {this.state.nombre}</p>
                </div>
            )
        } else {
            return (
                <div className="container">
                    <div>
                        <label>Introduce tu nombre</label>
                        <input
                            type="text"
                            name="nombre"
                            placeholder="Escribe aqui tu nombre"
                            autoFocus
                            onChange={this.getDatos}></input>
                    </div>
                    <button
                        className="btn btn-primary"
                        onClick={this.changeDiv}>
                        Envia</button>
                </div>
            )
        }
    }
}

export default Form;