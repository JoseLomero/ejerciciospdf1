import React, { Component } from 'react';

import InformacionClima from './components/InformacionClima';
import FormularioClima from './components/FormularioClima';

import { API_KEY } from './keys';


class App extends Component {

    state = {
        list: {
            temperature: '',
            description: '',
            humity: '',
            wind: '',
            city: '',
            estado: '',
            icon: '',
            error: null
        },
        form: {
            city: '',
            country: ''
        }

    }

    getDatos = async e => {
        e.preventDefault();

        const WEATHER_FUNCTION = "weather";
        const WEATHER_APIURL = `http://api.openweathermap.org/data/2.5/${WEATHER_FUNCTION}?q=${this.state.form.city},
                                ${this.state.form.country}&APPID=${API_KEY}`;
        const respuesta = await fetch(WEATHER_APIURL);
        const datos = await respuesta.json();

        this.setState({
            list: {
                temperature: datos.main.temp,
                description: datos.weather[0].description,
                humity: datos.main.humidity,
                wind: datos.wind.speed,
                city: datos.name,
                estado: datos.sys.country,
                icon: datos.weather[0].icon,
                error: null
            }
        })
    }

    handleOnChange = (e) => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })

    }

    render() {
        return (
            <div className="container p-4 bg-dark">
                <div className="row justify-content-center">
                    <h1 className="card-title text-light">El Tiempo</h1>
                </div>
                <div className="row align-items-center justify-content-center">
                    <div className="col-md-6 align-self-center">
                        <div>
                            <FormularioClima origen={this.getDatos} onChange={this.handleOnChange} formValues={this.state.form} />
                        </div>
                        <div>
                            <InformacionClima listValues={this.state.list} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;