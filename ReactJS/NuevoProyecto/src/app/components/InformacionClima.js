import React from 'react';

const InformacionClima = props => {

    let urlIcono = "http://openweathermap.org/img/w/" + props.listValues.icon + ".png";
    // let rightTemperature = {props.listValues.temperature} - 173;

    return (
        <div className="row">
            {
                props.error &&
                <div className="alert alert-danger">
                    <p>props.error</p>
                </div>
            }
            {props.listValues.temperature ?
                <div className="card card-body">
                    <p>Ubicación: {props.listValues.city}, {props.listValues.estado}</p>
                    <p>Humedad: {props.listValues.humity}%</p>
                    <p>Temperatura: {Math.floor(props.listValues.temperature - 273)}&deg;C, {props.listValues.description}</p>
                    <p>Viento: {props.listValues.wind}</p>
                    <p>Clima: <img id="wicon" src={urlIcono} alt="Weather Icon" /></p>
                </div>
                :
                <div></div>
            }
        </div>
    )
}

export default InformacionClima;