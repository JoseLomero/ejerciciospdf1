import React from 'react';

const FormularioClima = props => (
    <div className=".card car-body">
        <form action="" onSubmit={props.origen}>
            <div className="form-group">
                <input
                    type="text"
                    name="city"
                    placeholder="Nombre de la ciudad"
                    className="form-control" autoFocus
                    value={props.formValues.city}
                    onChange={props.onChange}
                />
            </div>
            <div className="form-group">
                <input
                    type="text"
                    name="country"
                    placeholder="Nombre del pais"
                    className="form-control"
                    value={props.formValues.country}
                    onChange={props.onChange}

                />
            </div>
            <div>
                <button className="btn btn-info btn-block">
                    Obtén información
                    </button>
            </div>
        </form>
    </div>
)

export default FormularioClima;